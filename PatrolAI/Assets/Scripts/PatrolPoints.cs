﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PatrolPoints : MonoBehaviour
{
    public int NoOfEnemyNear = 0;

	// Use this for initialization
	void Start ()
    {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            NoOfEnemyNear++;

            //if its about to patrol or just potrol by another enemy bot then skip this bots patrol
            if (NoOfEnemyNear > 1)
            {
                other.gameObject.GetComponentInParent<Patrol>().GotoNextPoint();
            }


          
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
            NoOfEnemyNear--;
    }
}
