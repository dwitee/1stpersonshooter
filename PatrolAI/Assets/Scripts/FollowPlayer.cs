﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */

using UnityEngine;
using System.Collections;

/// <summary>
/// Follow player.
/// </summary>
[RequireComponent(typeof(NavMeshAgent))]
public class FollowPlayer : MonoBehaviour {

    //public Transform Player;
    public GameObject Player;
	NavMeshAgent navMeshAgent;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	void Awake ()
	{

		navMeshAgent = GetComponent<NavMeshAgent> ();
        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player == null)
            Debug.LogWarning(" The Player is not present in scene");
    }

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
        if ( Player!= null)
		    navMeshAgent.SetDestination (Player.transform.position);
	}
}
