﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    private float lifeTime = 4.0f;

	// Use this for initialization
	void Start ()
    {
        Destroy(this, lifeTime);
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}


	/// <summary>
	/// Raises the trigger enter event.
	/// </summary>
	/// <param name="other">Other.</param>
    void OnTriggerEnter( Collider other)
    {
        if (other.GetComponentInParent<Enemy>() != null)
              other.GetComponentInParent<Enemy>().HitByBullet();
		else if (other.gameObject.CompareTag("Obstacle"))
            Destroy(gameObject);
    }
}
