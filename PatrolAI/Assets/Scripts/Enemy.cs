﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Enemy.
/// </summary>
public class Enemy : MonoBehaviour {

    public GameObject Player;
    public float EnemySeeRange = 10.0f;
    public float EnemyRange = 5.0f;
    public bool NoObstacleInBetween = false;
    public float EnemyHealth = 50.0f;
    public bool IsHit = false;
    public Rigidbody Bullet;
    public float BulletSpeed = 200;

    public enum States
    {
        IDLE,
        PATROLING,
        ATTACKING,
        FIRING,
        DEAD
    };

    public States enemyState;

    // Use this for initialization
    void Start ()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
        if (Player == null)
            Debug.LogWarning(" The Player is not present in scene");

        enemyState = States.IDLE;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //just to show forward direction
        Debug.DrawLine(transform.position, transform.position + transform.forward, Color.cyan);

        EnemyAI();

        //if health zero its dead
        if (EnemyHealth < 0)
            Kill();
            
    }

    /// <summary>
    /// To patrol or when player seen follow player
    /// </summary>
    void EnemyAI()
    {
        Vector3 dist = Player.transform.position - transform.position;
        //if payer is in FOV or within range follow the player and no obstacle between player and enemy
		if ((NoObstacleInBetween == true ) && (CheckIfPlayerInFOV() == true || dist.sqrMagnitude < EnemyRange * EnemyRange || IsHit == true) )
        {

            //Attack player
            AttackPlayer();   

        }
        else
        {
            //Regular patrol
            DoRegularPatrol();
            
        }

    }


    /// <summary>
    /// attack and follow player
    /// </summary>
    void AttackPlayer()
    {
		if (enemyState != States.ATTACKING )

        {
            enemyState = States.ATTACKING;
            Debug.DrawLine(Player.transform.position, transform.position, Color.yellow);
            //Move towards target
            GetComponent<Patrol>().enabled = false;
            GetComponent<FollowPlayer>().enabled = true;
            StartGlow();
            InvokeRepeating("Fire", 2.0f, 1.0f);

        }

    }

    /// <summary>
    /// Do regular patrol
    /// </summary>
    void DoRegularPatrol()
    {

        if (enemyState != States.PATROLING)
        {
            enemyState = States.PATROLING;
            GetComponent<Patrol>().enabled = true;
            GetComponent<FollowPlayer>().enabled = false;
            StopGlow();
            CancelInvoke();
        }

    }

    /// <summary>
    /// check if player seen in enemy FOV
    /// </summary>
    /// <returns></returns>
    bool CheckIfPlayerInFOV()
    {
        //this can be done by simple dot product operation 

        // get the normalised difference vector
        Vector3 diff = Player.transform.position - transform.position;

        //if player is far away from visible range
        if (diff.sqrMagnitude > EnemySeeRange * EnemySeeRange)
        {
            return false;
        }
        diff.Normalize();

        //get dot product of the forward vector of enemy and diff vector between enemy and player
        float dotProduct = Vector3.Dot(diff, transform.forward);

        //lets say field of view angle is 180 degree and the enemy can see the player if he is in forward direction with a FOV angle 180
        //then dotProct value will be between 0 and 1, if its negative then enemy is looking away from player

        if (dotProduct > 0)
        {
            //Debug.DrawLine(Player.transform.position, transform.position, Color.red);
            return true;
        }
        else
        {
            return false;
        }
    }


    /// <summary>
    /// util function to glow an object , to be moved to util class
    /// </summary>
    void StartGlow()
    {
        Color lerpedColor = Color.Lerp(Color.white, Color.blue, Mathf.PingPong(Time.time, 1));
        for (int i = 0; i < GetComponentInChildren<Renderer>().materials.Length; i++)
        {

            // 2.d: Uncomment the below line to highlight the material when gaze enters.
            GetComponentInChildren<Renderer>().materials[i].SetFloat("_EmissionColor", 1.0f);
            GetComponentInChildren<Renderer>().materials[i].SetColor("_EmissionColor", lerpedColor);
        }
    }

    /// <summary>
    /// util function
    /// </summary>
    void StopGlow()
    {
        for (int i = 0; i < GetComponentInChildren<Renderer>().materials.Length; i++)
        {
            // 2.d: Uncomment the below line to highlight the material when gaze enters.
            GetComponentInChildren<Renderer>().materials[i].SetFloat("_EmissionColor", .0f);
            GetComponentInChildren<Renderer>().materials[i].SetColor("_EmissionColor", Color.black);

        }
    }

    /// <summary>
    /// use ray cast to check obstacle
    /// </summary>
    void FixedUpdate()
    {
        RaycastHit hit;
        //Vector3 vec = transform.TransformDirection(Vector3.forward);

        Vector3 vec = Player.transform.position - transform.position;
        vec.Normalize();
        if (Physics.Raycast(transform.position, vec, out hit, 10))
        {
            if (hit.collider.GetComponentInParent<Player>() !=null)
            {
                Debug.Log(" Enemy can see the player No Obstacles");
                NoObstacleInBetween = true;
            }
            else
            {
                NoObstacleInBetween = false;
            }


        }
    }

	/// <summary>
	/// Hits the by bullet.
	/// </summary>
    public void HitByBullet()
    {
        IsHit = true;
        EnemyHealth -= 25.0f;
		enemyState = States.IDLE;
		NoObstacleInBetween = true; // if hit by player then in this frame player can obviously see directly
        // if Hit by Bullet Rotate towards incoming bullet and attack player
        //take Revenge
         AttackPlayer();
    }

    /// <summary>
    /// kill
    /// </summary>
    public void Kill()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// Fire Bullet
    /// </summary>
    void Fire()
    {

        Rigidbody instantiatedProjectile = Instantiate(Bullet, transform.position + transform.forward,
                                                       transform.rotation)
         as Rigidbody;

        instantiatedProjectile.velocity = transform.forward * BulletSpeed;
        instantiatedProjectile.gameObject.tag = "EnemyBullet";
    }
}
