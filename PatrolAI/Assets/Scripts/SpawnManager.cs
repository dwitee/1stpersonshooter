﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Spawn manager.
/// </summary>
public class SpawnManager : MonoBehaviour {


    public GameObject enemy;                // The enemy prefab to be spawned.
    public float spawnTime = 0.3f;            // How long between each spawn.
    public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
    public GameObject PatrolPoints;
    public int MaxEnemyCount = 10;
    private static int CurrentEnemyCount = 0;

    void Start()
    {
        //Get all the patrol point and use them as spwn points
        PopulateSpawnPoints();
            // Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }


/// <summary>
/// 
/// </summary>
    void PopulateSpawnPoints()
    {
        if (PatrolPoints != null)
        {
            Transform[] points = PatrolPoints.GetComponentsInChildren<Transform>(true);
            //make space in the array
            spawnPoints = new Transform[points.Length];

            //assign all patrol points to spawn points
            spawnPoints = points;
         
        }
    }

	/// <summary>
	/// Spawn enemies at the patrol point random order.
	/// </summary>
    void Spawn()
    {
        // If the player has no health left...
        if (CurrentEnemyCount > MaxEnemyCount)
            return;

        // Find a random index between zero and one less than the number of spawn points.
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);

        // Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
        GameObject clone = (GameObject)  Instantiate(enemy, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation);

        //once intiated make the dest patrol point as the nearest patrol point to its current location
        clone.GetComponent<Patrol>().StartPatrolPoint = spawnPointIndex;
        CurrentEnemyCount++;
    }
   
	
	// Update is called once per frame
	void Update () {
	
	}
}
