﻿
/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;

/// <summary>
/// Shoot.
/// </summary>
public class Shoot : MonoBehaviour
{

    public Rigidbody projectile;

    public float speed = 200;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if ( Input.GetKeyDown(KeyCode.Space))
        {
            Fire();
        }
    }


    /// <summary>
    /// Fire Bullet
    /// </summary>
    void Fire()
    {
        Rigidbody instantiatedProjectile = Instantiate(projectile, transform.position + transform.forward,
                                                       transform.rotation)
         as Rigidbody;

        instantiatedProjectile.velocity = transform.forward * speed;
        instantiatedProjectile.gameObject.tag = "PlayerBullet";
    }
}
