﻿/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;


/// <summary>
/// Patrol all patrol points
/// </summary>
public class Patrol : MonoBehaviour
{
    public Transform[] PatrolPoints;
    public  int  StartPatrolPoint  = 0;
    public GameObject PatrolPointsGO;
    private NavMeshAgent agent;

    void Start()
    {
     
        PopulateSpawnPoints();
        agent = GetComponent<NavMeshAgent>();

        // Disabling auto-braking allows for continuous movement
        // between points (ie, the agent doesn't slow down as it
        // approaches a destination point).
        agent.autoBraking = false;

        GotoNextPoint();
    }


    void PopulateSpawnPoints()
    {
        if (PatrolPointsGO != null)
        {
            Transform[] points = PatrolPointsGO.GetComponentsInChildren<Transform>(true);
            //make space in the array
            PatrolPoints = new Transform[points.Length];

            //assign all patrol points to spawn points
            PatrolPoints = points;

        }
    }

    public  void GotoNextPoint()
    {
        //return if agent not yet on the nav mesh
        if (agent.isOnNavMesh == false)
            return;

        // Returns if no points have been set up
        if (PatrolPoints.Length == 0)
            return;

        // Set the agent to go to the currently selected destination.
        agent.destination = PatrolPoints[StartPatrolPoint].position;

        // Choose the next point in the array as the destination,
        // cycling to the start if necessary.
        StartPatrolPoint = (StartPatrolPoint + 1) % PatrolPoints.Length;
    }


    void Update()
    {
        //return if agent not yet on the nav mesh
        if (agent.isOnNavMesh == false)
            return;
        // Choose the next destination point when the agent gets
        // close to the current one.
        if ( agent.remainingDistance < 0.5f)
            GotoNextPoint();

    }

}
