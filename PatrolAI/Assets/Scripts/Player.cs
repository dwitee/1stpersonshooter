/* Copyright (C) Dwitee Krishna Panda - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Dwitee Krishna Panda <dwitee@gmail.com>
 */
using UnityEngine;
using System.Collections;


/// <summary>
/// Player.
/// </summary>
public class Player : MonoBehaviour {
	public float movementSpeed = 10;
	public float turningSpeed = 90;

	void Update()
    {
        Debug.DrawLine(transform.position, transform.position + transform.forward, Color.cyan);

        float sideWise = Input.GetAxis("Horizontal") * movementSpeed * Time.deltaTime;
		transform.Translate(sideWise, 0, 0);
		
		float forward = Input.GetAxis("Vertical") * movementSpeed * Time.deltaTime;
		transform.Translate(0, 0, forward);
	}
}