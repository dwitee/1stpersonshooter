

Game Architecture:
Setting up the walkable area and obstacles using Navigation static  and  NavMeshAgent for all bots and Player

Enemy.cs:

Enemy AI: 
1. If the player position is within EnemySeeRange and is seen by the enemy ( calculation done by basic dot product of enemy forward vector and diff vector of player and enemy)
Or if Player is too close i.e within EnemyRange then the Enemy stop patrol and follow the Player ( Attack the player)

2. If the player moves away from the enemy range or away from the vision of enemy then Enemy goes back to Patroling.
3. When the Player is seen by a particular enemy its indicated by glowing/highlighting the enemy. when player is out of range and vision then the highlight stops.
4. When The is a obstacle or wall in between player and enemy's view then the player should not be seen by enemy. Done by RayCasting
5. If enemy is hit by players bullet then enemy comes to the place from where the fire was shot if enemy is near by that place then it hunts the player and attacks .


SpawnManager:

The bots will be spawned randomly on each patrol points and they will start navigating towards the next patrol point.
This will evenly ditribute the enemey bots across all the patrol points and make the enemy spawn un predictable

Patrols Points:
when a bot is already near a patrol point it will instruct any other bot coming towards it to skip this point as already a bot is here. 
This will help in uniform distribution of enemies through out the patrol points
This will be placed on any location by the designer or programmer 
 
SpawnManager.cs:
This spawns the enemies till MaxEnemyCount is reached and position each enemy randomly on different patrol points.
Then instructs the enemies to walk to its subsequent patrol points

Patrol.cs:
This controls the enemies movement to the patrol points. 

Camera control:
FollowCamera.cs:
Camera is always folowing the target ( Player) in LateUpdate()
TODO: when a wall comes between camera and player make the wall transparent for better view

Shoot.cs :
Fires Bullet on pressing keyboard.space
HitDamage is 25% of Enemy health.

Note: Please bake the NavMesh before running the Game( Windows->Navigation-> Bake)


